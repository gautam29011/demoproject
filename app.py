from flask import Flask, jsonify, request
import os
import indian_names
import pandas as pd 

app = Flask(__name__)


def generate_data(x):
    data = pd.DataFrame()
    for i in range(1, x + 1):  
        name = indian_names.get_full_name()
        data.loc[i, 'id'] = int(i) 
        data.loc[i, 'name'] = name
        email = name.lower().replace(' ', '.') + "@gmail.com"
        data.loc[i, 'email'] = email
        data.loc[i, 'country'] = 'India'
    return data


@app.route("/bot", methods=["POST"])
def player_bot():
    try:
        user = request.args.get("user")

        if user is None:
            return jsonify({"error": "User parameter is missing"}), 400

        try:
            user = int(user)
        except ValueError:
            return jsonify({"error": "Invalid user parameter. Must be an integer."}), 400

        user_data = generate_data(user)

        if user_data is None:
            return jsonify({"error": "Something went wrong"}), 500
        else:
            data = user_data.to_json(orient='records')
            return data, 200, {'Content-Type': 'application/json'}
    
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    

if __name__ == "__main__":
    app.run(debug=True, port=os.getenv("PORT", default=5000))
